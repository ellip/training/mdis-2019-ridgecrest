## mdis-2019-ridgecrest

MDIS-2019 - Ridgecrest July 2019 event analysis

Training material for MDIS 2019

### Getting the experiment

This experiment is hosted in a software repository.

Use `git` to clone it:

```bash
cd /workspace
git clone https://gitlab.com/ellip/training/mdis-2019-ridgecrest.git
cd mdis-2019-ridgecrest
```

### Configuring the Python conda environment

The file `environment.yml` contains the Python conda environment for running the notebooks contained in this folder.

From the shell, run:

```bash
conda env create --file=environment.yml
```

Once the environment configuration is done, you can activate it:

```bash
conda activate mdis-2019-ridgecrest
```

### Running the training kit notebooks

Open the `README.ipynb` notebook to understand the training kit structure.

Open each of the notebooks and update the kernel to use `mdis-2019-ridgecrest`

Run the notebook by executing each of the cells with `shift` + `Enter`.

In step 00, provide your Ellip username and associated Ellip API key.

### Improving the training in a development branch

This experiment is under version control and uses the git flow method (see [https://datasift.github.io/gitflow/IntroducingGitFlow.html])

If not done previously, clone the experiment repository:

```bash
git clone https://gitlab.com/ellip/training/mdis-2019-ridgecrest.git
cd mdis-2019-ridgecrest
```

Then, checkout the `develop` branch with:

```bash
git checkout develop
```

At this stage, update the experiment.

When done:

```bash
git add -A
git commit -m '<commit message>'
git pull
```

Finally, do a release with:

```bash
ciop-release
```





