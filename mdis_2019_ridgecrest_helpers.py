import os

from datetime import datetime, timedelta
from datetime import date
import dateutil.parser
import time
from time import sleep
import sys
import imp
import requests
import json
from shapely.geometry import box
from shapely.wkt import loads
from shapely.geometry import shape
import logging
import geopandas as gpd
import pandas as pd
import cioppy
from owslib.wps import WebProcessingService

class EarthQuake:

    def __init__(self, record):
       # print(record)
        self.usgs_api = 'https://earthquake.usgs.gov/fdsnws/event/1/query'

        self.wkt = 'POINT({} {})'.format(record['geometry']['coordinates'][0],
                                         record['geometry']['coordinates'][1])

        self.depth = record['geometry']['coordinates'][2]
        self.date = datetime.strftime(datetime.fromtimestamp(record['properties']['time'] /1000.0), 
                                      '%Y-%m-%dT%H:%M:%SZ')
        
        self.updated = datetime.strftime(datetime.fromtimestamp(record['properties']['updated'] /1000.0),
                                         '%Y-%m-%dT%H:%M:%SZ')
        
        self.url = str(record['properties']['url'])
        self.title = str(record['properties']['title'])

        try:
            self.abstract = self.title + '. ' + str(record['properties']['products']['impact-text'][0]['contents']['']['bytes']).rstrip()
        except:
            self.abstract = self.title

        self.id = str(record['id'])

        self.pgv_aoi = None

        self.epicentre_aoi = self.get_epicentre_aoi()

        try:
            self.detailsUrl = str(record['properties']['detail'])
        except (KeyError,AttributeError):
            self.detailsUrl = None

        try:

            self.shake_map_url = str(record['properties']['products']['shakemap'][0]['contents']["download/cont_pgv.json"]['url'])
            self.has_shake_map = True
            self.pgv_aoi = self.get_pgv_aoi()

        except (KeyError, AttributeError):
            self.has_shake_map = False



        self.quakeml = requests.get('{}?eventid={}&format=quakeml'.format(self.usgs_api, self.id)).content


    def get_pgv_aoi(self, buffer_size=0.5):

        if self.has_shake_map:

            r = requests.get(self.shake_map_url)

            pgv = json.loads(r.content)

            pgv_dict = {}

            v = []
            geometry = []

            for index, feature in enumerate(pgv["features"]):
                v.append(feature['properties']['value'])
                geometry.append(shape(feature['geometry']).wkt)

            pgv_dict = {'pgv': v, 'geometry': geometry}

            gdf = gpd.GeoDataFrame(pgv_dict)

            gdf['geometry'] = gdf['geometry'].apply(loads)

            significant_pgv =  max(gdf['pgv'])  # sorted(list(gdf['pgv'].values))[len(list(gdf['pgv'].values)) // 2]

            selected_pgv = gdf[gdf['pgv'] == significant_pgv]['geometry'].values[0]

            self.pgv_aoi = box(*selected_pgv.buffer(buffer_size).bounds)

        return self.pgv_aoi

    def get_epicentre_aoi(self, buffer_size=0.5):

        return box(*loads(self.wkt).buffer(buffer_size).bounds)


    def get_moment_tensors(self):

        return self.get_product_type_properties("moment-tensor")


    def get_focal_mechanisms(self):

        return self.get_product_type_properties("focal-mechanism")


    def get_product_type_properties(self, product_type):

        if(product_type in self.types and self.detailsUrl):

            r = requests.get(self.detailsUrl)

            if r.status_code == 404 :
                return None

            record = json.loads(r.content.decode('utf-8'))
            return record['properties']['products'][product_type]

        elif(product_type in self.types and not self.detailsUrl):
            print ('{0} found but no url detail found'.format(product_type))
            return None

        return None


class EarthQuakes:

    def __init__(self):

        self.usgs_api = 'https://earthquake.usgs.gov/fdsnws/event/1/query'

    def search_id(self, id):
        self.earthquakes = []

        payload = {'eventid': id,
                   'format': 'geojson'}

        r = requests.get(self.usgs_api, params = payload)

        record = json.loads(r.content.decode('utf-8'))

        self.count = 1

        self.earthquakes.append(EarthQuake(record))

    def search(self, start_time, end_time, min_mag=5, bbox=[-180, -90, 180, 90], min_depth=0, max_depth=100000):

        self.earthquakes = []

        min_lon, min_lat, max_lon, max_lat = bbox

        payload = {'format': 'geojson',
                    'starttime': start_time,
                    'endtime': end_time,
                    'minmagnitude': str(min_mag),
                    'minlongitude' : str(min_lon),
                    'minlatitude' : str(min_lat),
                    'maxlongitude' : str(max_lon),
                    'maxlatitude' : str(max_lat),
                    'mindepth' : str(min_depth),
                    'maxdepth' : str(max_depth)}

        r = requests.get(self.usgs_api, params=payload)

        print(r.status_code)

        self.json = r.json()

        results = json.loads(r.content.decode('utf-8'))

        self.count = results['metadata']['count']

        self.earthquakes = []

        for eq in results['features']:

            self.earthquakes.append(EarthQuake(eq))


def analyse(row, aoi_wkt):

    aoi = loads(aoi_wkt)

    aoi_intersection = (row.geometry.intersection(aoi).area / aoi.area) * 100

    series = dict([('aoi_intersection', aoi_intersection)])

    return pd.Series(series)

def analyse_s2(row, aoi_wkt, event_date):

    aoi = loads(aoi_wkt)

    aoi_intersection = (row.geometry.intersection(aoi).area / aoi.area) * 100

    temporal_baseline = round(divmod((event_date - dateutil.parser.parse(row.startdate)).total_seconds(), 3600 * 24)[0] +
                              round(divmod((event_date - dateutil.parser.parse(row.startdate)).total_seconds(), 3600 * 24)[1])/(3600 * 24))

    tile = row.identifier[38:44]

    series = dict()

    series['aoi_intersection'] = aoi_intersection
    series['tile'] = tile
    series['temporal_baseline'] = temporal_baseline

    return pd.Series(series)


def analyse_s1(row, aoi_wkt, event_date):

    aoi = loads(aoi_wkt)

    aoi_intersection = (row.geometry.intersection(aoi).area / aoi.area) * 100

    temporal_baseline = round(divmod((event_date - dateutil.parser.parse(row.startdate)).total_seconds(), 3600 * 24)[0] +
                              round(divmod((event_date - dateutil.parser.parse(row.startdate)).total_seconds(), 3600 * 24)[1])/(3600 * 24))


    series = dict()

    series['aoi_intersection'] = aoi_intersection
    series['temporal_baseline'] = temporal_baseline

    return pd.Series(series)

def analyse_masters(row, slave, aoi_wkt):

    aoi = loads(aoi_wkt)

    slave_wkt =  slave.geometry
    slave_date = dateutil.parser.parse(slave.startdate)

    slave_coverage = row.geometry.area / slave_wkt.area * 100
    aoi_intersection = (row.geometry.intersection(aoi).area / aoi.area) * 100
    temporal_baseline = round(divmod((slave_date - dateutil.parser.parse(row.startdate)).total_seconds(), 3600 * 24)[0] +
                              round(divmod((slave_date - dateutil.parser.parse(row.startdate)).total_seconds(), 3600 * 24)[1])/(3600 * 24))

    series = dict([('slave_coverage', slave_coverage),
                  ('aoi_intersection', aoi_intersection),
                  ('temporal_baseline', temporal_baseline)])

    return pd.Series(series)


def download(row, api_key):
    headers = {'X-JFrog-Art-Api': api_key}
    #headers = {'Authorization': 'Bearer %s' % api_key,
    #       'User-Agent': 'curl/t2Client'}

    r = requests.get(row.enclosure, headers=headers)

    filename = row.enclosure[row.enclosure.rfind('/')+1:]

    print('Download {} as {}'.format(row.enclosure, filename))

    open(filename, 'wb').write(r.content)

    series = dict([('local_filename', filename)])

    return pd.Series(series)



def get_master(row):

    series = 'https://catalog.terradue.com/sentinel1/search'

    master_search_params = dict([('geom', row.geometry.wkt),
                             ('track', row.track),
                             ('pt', row.productType),
                             ('psn', row.platform),
                             ('start', datetime.strftime((dateutil.parser.parse(row.startdate) + timedelta(days=-12)), '%Y-%m-%dT%H:%M:%SZ')),
                             ('stop', datetime.strftime((dateutil.parser.parse(row.startdate) + timedelta(days=-1)), '%Y-%m-%dT%H:%M:%SZ')),
                             ('do', 'terradue')])


    master_search = gpd.GeoDataFrame(cioppy.Cioppy().search(end_point=series,
                                           params=master_search_params,
                                           output_fields='self,productType,track,enclosure,identifier,wkt,startdate,platform',
                                           model='EOP',
                                                   timeout=50000))

    master_search['geometry'] = master_search['wkt'].apply(loads)
    master_search = master_search.drop(columns=['wkt'])

    master_search = master_search.merge(master_search.apply(lambda row_master: analyse(row_master, row.geometry.wkt), axis=1),
              left_index=True,
              right_index=True)


    best_master = master_search[master_search.aoi_intersection == master_search.aoi_intersection.max()].iloc[0]

    #print(type(best_master))

    #print(best_master)
    series = dict()

    series['master_self'] = best_master.self
    series['master_track'] = best_master.track
    series['master_startdate'] = best_master.startdate
    series['master_platform'] = best_master.platform
    series['master_identifier'] = best_master.identifier

    return pd.Series(series)


def get_parameters_as_dict(wps_url, process_id):
    """This function returns a dictionary with the WPS parameters and their associated default values

    Args:
        wps_url: the WPS end-point.
        process_id: the process identifier

    Returns
        A dictionary with the parameters as keys .

    Raises:
        None.
    """
    wps = WebProcessingService(wps_url, verbose=False, skip_caps=True)

    process = wps.describeprocess(process_id)

    data_inputs = dict()

    for data_input in process.dataInputs:
        # TRIGGER-15
        if data_input.identifier == '_T2Username' or data_input.identifier == '_T2ApiKey': continue

        if (data_input.minOccurs != 0) or (data_input.identifier == 't2_coordinator_name') or (data_input.identifier == '_T2Username'):
            if data_input.defaultValue is None:
                data_inputs[data_input.identifier] = ''
            else:
                data_inputs[data_input.identifier] = data_input.defaultValue

    return data_inputs


def check_status(row, creds):

    ciop = cioppy.Cioppy()

    search = ciop.search(end_point=row.self,
                params=[],
                output_fields='self,cat,link:results',
                model='GeoTime',
                creds=creds)[0]

    series = dict()

    series['status'] = filter(lambda element: 'source' in element, search['cat'].split(';'))[0]
    series['link_results'] = search['link:results']

    return pd.Series(series)